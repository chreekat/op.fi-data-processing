#!/usr/bin/env runhaskell

import System.IO

main = do
    hSetEncoding stdin latin1
    putStrLn . unlines . map simplifyLine . drop 1 . lines =<< getContents

simplifyLine l =
    let (date:_:amt:_) = splitOn1 ';' l
        [d,m,y] = splitOn1 '.' date
        [eur,cent] = splitOn1 ',' amt
    in concat
        [ y
        , "/"
        , m
        , "/"
        , d
        , ","
        , eur
        , "."
        , cent
        ]

splitOn1 :: Char -> String -> [String]
splitOn1 delim = foldr snip [[]]
    where
    snip c acc@(sp:rest)
        | c == delim = ([]:acc)
        | otherwise = ((c:sp):rest)

-- (a `f` (b `f` (c `f` z)))
