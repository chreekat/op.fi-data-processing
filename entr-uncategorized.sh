#!/usr/bin/env bash

set -Eeuo pipefail

# Try `tmux splitw -l12 "./entr-uncategorized.sh print"`

cmd="hledger"
for i in transaktioner/*.csv; do
    cmd="${cmd} -f $i"
done
cmd="${cmd} --rules-file main.rules  $1 expenses:uncategorized | head -n 30"
ls *.rules transaktioner/*.csv $0 | entr -c -s "$cmd"

