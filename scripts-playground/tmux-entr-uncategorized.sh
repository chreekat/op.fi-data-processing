#!/usr/bin/env bash

set -Eeuo pipefail

tmux splitw -l 16 ./entr-uncategorized.sh print
tmux splitw -l 4 ./entr-uncategorized.sh bal
