transaktioner.ledger: transaktioner/*.csv *.rules
	rm -f $@ transaktioner/.latest.transaktioner*.csv
	hledger -f $@ import --rules-file main.rules transaktioner/*.csv

.PHONY: processing
processing:
	tmux splitw -l16 "./entr-uncategorized.sh print"
	tmux splitw -l3 "./entr-uncategorized.sh bal"
